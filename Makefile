CFLAGS=-Wall

all: libpluto.a libpluto.so

.PHONY: clean

libpluto.a: pluto.o
	ar rcs libpluto.a pluto.o

libpluto.so: pluto.o
	gcc -shared -Wl,-soname,libpluto.so -o libpluto.so pluto.o


pluto.o: pluto.c pluto.h
	gcc $(CFLAGS) -c -fPIC pluto.c -o pluto.o

clean:
	rm -f pluto.o libpluto.{a,so}
