/**
 * This file is part of Pluto Personal Finance Library.
 *
 * Pluto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pluto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pluto.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pluto.h"

#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static sqlite3 *db = NULL;
static char error[MAX_ERROR_MESSAGE_SIZE + 1];

struct callback_string {
    char *list;
    size_t size;
    const char *separator;
};

/* Creates the main tables if not exits and set default settings. */
static int init_db()
{
    char *msg;
    const char query[] =
        /* Creates account table. */
        "CREATE TABLE IF NOT EXISTS Account ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "name TEXT UNIQUE"
        ");"
        /* Creates tag table. */
        "CREATE TABLE IF NOT EXISTS Tag ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "name TEXT UNIQUE"
        ");"
        /* Creates tracking table. */
        "CREATE TABLE IF NOT EXISTS Tracking ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "date INTEGER NOT NULL,"
            "account INTEGER NOT NULL,"
            "description TEXT NOT NULL,"
            "tag INTEGER DEFAULT NULL,"
            "quantity REAL NOT NULL,"
            "value REAL NOT NULL,"
            /* Tracking constraints. */
            "CHECK (quantity > 0),"
            "FOREIGN KEY (account) REFERENCES Account(id) ON DELETE CASCADE,"
            "FOREIGN KEY (tag) REFERENCES Tag(id) ON DELETE SET NULL"
        ");"
        /* Enables foreign keys. */
        "PRAGMA foreign_keys = ON;";

    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

int pl_open(const char *filename)
{
    int retval = sqlite3_open(filename, &db);

    if (retval)
        strcpy(error, "pluto could not open the database");
    else
        retval = init_db();

    if (retval)
    {
        sqlite3_close(db);
        db = NULL;
    }

    return retval;
}

static int check_opened()
{
    if (db == NULL)
    {
        strcpy(error, "database is not opened");
        return 1;
    }
    return 0;
}

int pl_close()
{
    int retval;

    if (check_opened())
        return 1;

    retval = sqlite3_close(db);
    if (retval != SQLITE_OK)
    {
        strcpy(error, "pluto could not close the database");
        return 1;
    }

    db = NULL;
    return 0;
}

int pl_is_opened()
{
    return db != NULL;
}

int pl_account_create(const char *account)
{
    char *msg;
    char query[50 + MAX_ACCOUNT_NAME_SIZE];
    size_t len = strlen(account);

    if (check_opened())
        return 1;

    if (len < 1 || len > MAX_ACCOUNT_NAME_SIZE)
    {
        strcpy(error, "account name is too long or too short");
        return 1;
    }

    sprintf(query, "INSERT INTO Account(name) VALUES ('%s');", account);
    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

int pl_account_delete(const char *account)
{
    char *msg;
    char query[50 + MAX_ACCOUNT_NAME_SIZE];

    if (check_opened())
        return 1;

    sprintf(query, "DELETE FROM Account WHERE name = '%s';", account);
    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

int pl_account_rename(const char *account, const char *name)
{
    char *msg;
    char query[50 + 2 * MAX_ACCOUNT_NAME_SIZE];
    size_t len = strlen(name);

    if (check_opened())
        return 1;

    if (len < 1 || len > MAX_ACCOUNT_NAME_SIZE)
    {
        strcpy(error, "account name is too long or too short");
        return 1;
    }

    sprintf(query, "UPDATE Account SET name = '%s' WHERE name = '%s';",
            name, account);

    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

static int
exists_callback(void *exists, int argc, char *argv[], char *cols[])
{
    *((int *) exists) = 1;
    return 0;
}

int pl_account_exists(const char *account)
{
    char *msg;
    char query[50 + MAX_ACCOUNT_NAME_SIZE];
    size_t len = strlen(account);
    int exists = 0;

    if (check_opened())
        return 0;

    if (len < 1 || len > MAX_ACCOUNT_NAME_SIZE)
    {
        strcpy(error, "account name is too long or too short");
        return 1;
    }

    sprintf(query, "SELECT * FROM Account WHERE name = '%s';", account);

    if (sqlite3_exec(db, query, exists_callback, &exists, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
    }

    return exists;
}

int account_list_callback(void *_arg, int argc, char *argv[], char *cols[])
{
    struct callback_string *arg = (struct callback_string *) _arg;

    size_t len = strlen(argv[0]) + strlen(arg->separator);
    if (arg->size > len)
    {
        strcat(arg->list, argv[0]);
        strcat(arg->list, arg->separator);
        arg->size -= len;
    }

    return 0;
}

int pl_account_list(char *list, size_t size, const char *separator)
{
    char *msg;
    char query[] = "SELECT name FROM Account;";

    if (check_opened())
        return 0;

    if (list == NULL || size < 1 || separator == NULL)
    {
        strcpy(error, "invalid parameters");
        return 1;
    }

    list[0] = '\0';

    struct callback_string arg = {list, size - 1, separator};

    if (sqlite3_exec(db, query, account_list_callback, &arg, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 0;
    }

    return 0;
}

static int get_id_by_name_callback(void *id, int argc, char *argv[], char *cols[])
{
    *((int *) id) = atoi(argv[0]);
    return 0;
}

int pl_tracking_insert(pl_item_t *item)
{
    char query[100 + MAX_ACCOUNT_NAME_SIZE + MAX_TAG_NAME_SIZE + MAX_DESCRIPTION_SIZE];
    char tag[MAX_TAG_NAME_SIZE + 1];
    char *msg;
    int account;

    if (check_opened())
        return 1;

    if (!pl_account_exists(item->account))
    {
        strcpy(error, "invalid account name");
        return 1;
    }

    /* Gets account id by using its name. */
    sprintf(query, "SELECT id FROM Account WHERE name = '%s';", item->account);
    if (sqlite3_exec(db, query, get_id_by_name_callback, &account, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    /* Checks tag name. */
    if (item->tag[0] == '\0')
    {
        strcpy(tag, "NULL");
    }
    else
    {
        int tag_id;

        /* Always tries to insert the tag. */
        sprintf(query, "INSERT INTO Tag(name) VALUES ('%s');", item->tag);
        sqlite3_exec(db, query, NULL, NULL, NULL);

        /* Gets tag id by using its name. */
        sprintf(query, "SELECT id FROM Tag WHERE name = '%s';", item->tag);
        if (sqlite3_exec(db, query, get_id_by_name_callback, &tag_id, &msg))
        {
            strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
            error[MAX_ERROR_MESSAGE_SIZE] = '\0';
            sqlite3_free(msg);
            return 1;
        }

        sprintf(tag, "%d", tag_id);
    }

    /* Finally, inserts the item. */
    sprintf(query, "INSERT INTO Tracking(date, account, description, tag, quantity, value)"
            "VALUES (%lld, %d, '%s', %s, %f, %f);",
            (long long) item->date, account, item->description,
            tag, item->quantity, item->value);
    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    /* Gets the id. */
    sprintf(query, "SELECT seq FROM SQLITE_SEQUENCE WHERE name = 'Tracking';");
    if (sqlite3_exec(db, query, get_id_by_name_callback, &item->id, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

int pl_tracking_update(const pl_item_t *item)
{
    char query[100 + MAX_ACCOUNT_NAME_SIZE + MAX_TAG_NAME_SIZE + MAX_DESCRIPTION_SIZE];
    char tag[MAX_TAG_NAME_SIZE + 1];
    char *msg;
    int account;

    if (check_opened())
        return 1;

    if (!pl_account_exists(item->account))
    {
        printf("**%s\n", item->account);
        strcpy(error, "invalid account name");
        return 1;
    }

    // FIXME error if item doesn't exists.

    /* Gets account id by using its name. */
    sprintf(query, "SELECT id FROM Account WHERE name = '%s';", item->account);
    if (sqlite3_exec(db, query, get_id_by_name_callback, &account, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    /* Checks tag name. */
    if (item->tag[0] == '\0')
    {
        strcpy(tag, "NULL");
    }
    else
    {
        int tag_id;

        /* Always tries to insert the tag. */
        sprintf(query, "INSERT INTO Tag(name) VALUES ('%s');", item->tag);
        sqlite3_exec(db, query, NULL, NULL, NULL);

        /* Gets tag id by using its name. */
        sprintf(query, "SELECT id FROM Tag WHERE name = '%s';", item->tag);
        if (sqlite3_exec(db, query, get_id_by_name_callback, &tag_id, &msg))
        {
            strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
            error[MAX_ERROR_MESSAGE_SIZE] = '\0';
            sqlite3_free(msg);
            return 1;
        }

        sprintf(tag, "%d", tag_id);
    }

    /* Finally, updates the item. */
    sprintf(query, "UPDATE Tracking SET date = %lld, account = %d, description = '%s',"
            "tag = %s, quantity = %f, value = %f WHERE id = %d;",
            (long long) item->date, account, item->description,
            tag, item->quantity, item->value, item->id);
    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

int pl_tracking_remove(int id)
{
    char query[50];
    char *msg;

    if (check_opened())
        return 1;

    // FIXME error if item doesn't exists.

    sprintf(query, "DELETE FROM Tracking WHERE id = %d;", id);
    if (sqlite3_exec(db, query, NULL, NULL, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

static int fill_list_callback(void *_list, int argc, char *argv[], char *cols[])
{
    pl_item_list_t *list = (pl_item_list_t *) _list;

    if (list->size < list->capacity)
    {
        pl_item_t *item = list->itens + list->size;

        item->id = atoi(argv[0]);
        item->date = (time_t) atoll(argv[1]);
        strncpy(item->account, argv[2], MAX_ACCOUNT_NAME_SIZE);
        item->account[MAX_ACCOUNT_NAME_SIZE] = '\0';
        strncpy(item->description, argv[3], MAX_DESCRIPTION_SIZE);
        item->description[MAX_DESCRIPTION_SIZE] = '\0';
        strncpy(item->tag, argv[4] ? argv[4] : "", MAX_TAG_NAME_SIZE);
        item->tag[MAX_TAG_NAME_SIZE] = '\0';
        item->quantity = strtof(argv[5], NULL);
        item->value = strtof(argv[6], NULL);

        list->size++;
    }

    return 0;
}

int pl_tracking_search(pl_item_list_t *list,
                       const char *account,
                       const char *tag,
                       pl_filter_t filter, ...)
{
    char *msg;
    char query[1024 + MAX_ACCOUNT_NAME_SIZE + MAX_TAG_NAME_SIZE] =
        "SELECT Tr.id, Tr.date, Ac.name, Tr.description, Ta.name, Tr.quantity, Tr.value "
        "FROM (Tracking AS Tr JOIN Account AS Ac ON Tr.account = Ac.id) "
            "LEFT OUTER JOIN Tag AS Ta ON Tr.tag = Ta.id ";
    char where[512 + MAX_ACCOUNT_NAME_SIZE + MAX_TAG_NAME_SIZE] = "";

    char buffer[100 + MAX_ACCOUNT_NAME_SIZE + MAX_TAG_NAME_SIZE];

    va_list vl;
    va_start(vl, filter);

    if (check_opened())
        return 1;

    if (account)
    {
        sprintf(buffer, "WHERE Ac.name = '%s'", account);
        strcat(where, buffer);
    }
    if (tag)
    {
        if (where[0] == '\0')
            sprintf(buffer, "WHERE Ta.name = '%s'", tag);
        else
            sprintf(buffer, "AND Ta.name = '%s'", tag);
        strcat(where, buffer);
    }

    switch(filter)
    {
    case PL_FILTER_MOST_RECENT:
        sprintf(buffer, " ORDER BY Tr.date DESC LIMIT %u;", va_arg(vl, unsigned));
        strcat(query, where);
        strcat(query, buffer);
        break;
    case PL_FILTER_BY_DATE:
        strcpy(error, "not yet implemented");
        return 1;
    default:
        strcpy(error, "invalid filter");
        return 1;
    }

    va_end(vl);

    if (sqlite3_exec(db, query, fill_list_callback, list, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    return 0;
}

static int report_callback(void *_arg, int argc, char *argv[], char *cols[])
{
    struct callback_string *arg = (struct callback_string *) _arg;

    int i;
    size_t len = 0;
    for (i = 0; i < argc; i++)
        if (argv[i] != NULL)
            len += strlen(argv[i]);
        else
            len += 6;
    len += argc * strlen(arg->separator);

    if (arg->size > len)
    {
        for (i = 0; i < argc; i++)
        {
            if (argv[i] != NULL)
                strcat(arg->list, argv[i]);
            else
                strcat(arg->list, "(none)");

            strcat(arg->list, arg->separator);
        }
        arg->size -= len;
    }

    return 0;
}

int pl_report(char *info, size_t size, const char *separators,
              pl_report_t report, ...)
{
    char *msg;
    char query[1024];
    va_list vl;
    struct callback_string results = {info, size - 1, separators};
    results.list[0] = '\0';

    va_start(vl, report);

    /* PL_REPORT_BALANCE, PL_REPORT_BALANCE_BY_TAG */
    time_t from, to;
    const char *account;

    if (check_opened())
        return 1;

    switch(report)
    {
    case PL_REPORT_BALANCE:
        from = va_arg(vl, time_t);
        to = va_arg(vl, time_t);

        sprintf(query, "SELECT Ac.name, SUM(Tr.value * Tr.quantity) "
                       "FROM Tracking AS Tr JOIN Account AS Ac ON Tr.account = Ac.id "
                       "WHERE Tr.date >= %lld AND Tr.date < %lld "
                       "GROUP BY Ac.name;",
                       (long long) from, (long long) to);
        break;
    case PL_REPORT_BALANCE_BY_TAG:
        from = va_arg(vl, time_t);
        to = va_arg(vl, time_t);
        account = va_arg(vl, const char *);

        if (account == NULL)
            sprintf(query, "SELECT Ac.name, Ta.name, SUM(Tr.value * Tr.quantity) "
                           "FROM (Tracking AS Tr JOIN Account AS Ac ON Tr.account = Ac.id) "
                           "LEFT OUTER JOIN Tag AS Ta ON Tr.tag = Ta.id "
                           "WHERE Tr.date >= %lld AND Tr.date < %lld "
                           "GROUP BY Ac.name, Ta.name "
                           "ORDER BY Ac.name, Ta.name;",
                           (long long) from, (long long) to);
        else
            sprintf(query, "SELECT Ac.name, Ta.name, SUM(Tr.value * Tr.quantity) "
                           "FROM (Tracking AS Tr JOIN Account AS Ac ON Tr.account = Ac.id) "
                           "LEFT OUTER JOIN Tag AS Ta ON Tr.tag = Ta.id "
                           "WHERE Tr.date >= %lld AND Tr.date < %lld AND Ac.name = '%s' "
                           "GROUP BY Ac.name, Ta.name "
                           "ORDER BY Ac.name, Ta.name;",
                           (long long) from, (long long) to, account);

        break;

    default:
        strcpy(error, "invalid report");
        return 1;
    }

    if (sqlite3_exec(db, query, report_callback, &results, &msg))
    {
        strncpy(error, msg, MAX_ERROR_MESSAGE_SIZE);
        error[MAX_ERROR_MESSAGE_SIZE] = '\0';
        sqlite3_free(msg);
        return 1;
    }

    va_end(vl);

    return 0;
}

char *pl_error(char *message, size_t size)
{
    strncpy(message, error, size);
    message[size] = '\0';
    return message;
}

pl_item_list_t *pl_item_list_create(pl_item_list_t *list, size_t capacity)
{
    list->itens = (pl_item_t *) malloc(capacity * sizeof(pl_item_t));
    list->capacity = capacity;
    list->size = 0;

    return list;
}

pl_item_list_t *pl_item_list_delete(pl_item_list_t *list)
{
    free(list->itens);
    list->itens = NULL;
    list->capacity = 0;
    list->size = 0;

    return list;
}
