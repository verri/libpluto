/**
 * This file is part of Pluto Personal Finance Library.
 *
 * Pluto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pluto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pluto.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLUTO_H
#define PLUTO_H

#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#define MAX_DESCRIPTION_SIZE    50
#define MAX_ACCOUNT_NAME_SIZE   20
#define MAX_TAG_NAME_SIZE       20

#define MAX_ERROR_MESSAGE_SIZE  1024

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Represents a item in the database.
 *
 * id:          Item identifier. Valid item identifiers are greater than 0.
 * date:        Item date in number of seconds since epoch (precision in seconds)
 * account:     Account name that item belongs.
 * description: Item description.
 * tag:         Item tag.
 * quantity:    How many/much items/of item.
 * value:       How much cost each arbitrary unit of item.
 */
typedef struct
{
    int id;
    time_t date;
    char account[MAX_ACCOUNT_NAME_SIZE + 1];
    char description[MAX_DESCRIPTION_SIZE + 1];
    char tag[MAX_TAG_NAME_SIZE + 1];
    float quantity;
    float value;
} pl_item_t;

/**
 * @brief Represents a list of itens.
 *
 * itens:       Item list itself.
 * capacity:    Item list maximum capacity (allocated memory for itens).
 * size:        How many item slots are occupied.
 */
typedef struct
{
    pl_item_t *itens;
    size_t capacity;
    size_t size;
} pl_item_list_t;

/**
 * @brief Search filters.
 *
 * PL_FILTER_MOST_RECENT (..., unsigned number):
 *      Returns the 'number' most recent itens.
 * PL_FILTER_BY_DATE (..., time_t from, time_t to):
 *      Returns all itens with date after 'from' (inclusive) and before 'to' (exclusive).
 */
typedef enum
{
    PL_FILTER_MOST_RECENT,
    PL_FILTER_BY_DATE
} pl_filter_t;

/**
 * @brief Report types.
 *
 * PL_REPORT_BALANCE (..., time_t from, time_t to): (account, balance)
 *      Returns balance of each account. It considers itens with date after 'from'
 *      (inclusive) and before 'to' (exclusive).
 * PL_REPORT_BALANCE_BY_TAG (..., const char *account, time_t from, time_t to):
 *          (account, tag, balance)
 *      Return balance of each account (or specified when account is not NULL) by tag. It
 *      considers itens with date after 'from' (inclusive) and before 'to' (exclusive).
 */
typedef enum
{
    PL_REPORT_BALANCE, /* time_t time_t */
    PL_REPORT_BALANCE_BY_TAG /* time_t time_t account */
} pl_report_t;

/**
 * @brief Opens/Creates the pluto database in 'filename'.
 *
 * @param filename Path to pluto database.
 *
 * @return Return 0 if successful, error code otherwise.
 */
int pl_open(const char *filename);

/**
 * @brief Closes the previously opened pluto database.
 *
 * @return Return 0 if successful, error code otherwise.
 */
int pl_close();

/**
 * @brief Verifies if there is a opened pluto database.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_is_opened();

/**
 * @brief Creates a new account with name 'account' in the opened database.
 *
 * @param account Name of the new account.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_account_create(const char *account);

/**
 * @brief Deletes the account with name 'account'.
 *
 * @param account Name of the account to be deleted.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_account_delete(const char *account);

/**
 * @brief Renames the account with name 'account' to 'name'.
 *
 * @param account Name of the account to be renamed.
 * @param name New name of the account.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_account_rename(const char *account, const char *name);

/**
 * @brief Verifies if the account with name 'account' exists.
 *
 * @param account Name of the account to be verified.
 *
 * @return Return 1 if it exists, 0 otherwise.
 */
int pl_account_exists(const char *account);

/**
 * @brief Lists all accounts in the previously opened database.
 *
 * @param list Buffer to store the account names.
 * @param capacity Capacity of the buffer in bytes.
 * @param separator Separator to be concatenated between account names.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_account_list(char *list, size_t capacity, const char *separator);

/**
 * @brief Inserts the 'item' item in the previously opened database.
 *
 * Inserts the item in the database and fill the field 'id' with the identifier associated
 * to this item.
 * If an error occurs, 'item' will not be modified.
 *
 * @param item Item to be inserted.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_tracking_insert(pl_item_t *item);

/**
 * @brief Updates the 'item' item in the previously opened database.
 *
 * Updates the item in the database. It uses only the field 'id' to identify the item. All
 * other fields are used to update the database.
 *
 * @param item Item to be updated.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_tracking_update(const pl_item_t *item);

/**
 * @brief Removes the item with identifier 'id'.
 *
 * @param id Identifier of the item to be deleted.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_tracking_remove(int id);

/**
 * @brief Searches database based on 'filter' and stores the result in 'list'.
 *
 * Searches database based on 'filter'. The results are appended at the end of 'list'. If
 * 'account' is not NULL, searches only itens in account 'account'. The same for parameter
 * 'tag'.
 *
 * @param list List to store the results.
 * @param account Account name or NULL.
 * @param tag Tag name or NULL.
 * @param filter Search filter.
 * @param ... Filter options.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_tracking_search(pl_item_list_t *list,
                       const char *account,
                       const char *tag,
                       pl_filter_t filter, ...);

/**
 * @brief Reports based on 'report'. The report is written in 'info'.
 *
 * @param info Buffer to store the report results.
 * @param capacity Capacity of buffer 'info' in bytes.
 * @param separator Separator to be inserted between report informations.
 * @param report Type of report to be used.
 * @param ... Report options.
 *
 * @return Return 1 if it is opened, 0 otherwise.
 */
int pl_report(char *info, size_t capacity, const char *separator,
              pl_report_t report, ...);

/**
 * @brief Access the message of the last occurred error.
 *
 * @param message Buffer to store the error message.
 * @param capacity Maximum capacity of buffer 'message'.
 *
 * @return Given buffer.
 */
char *pl_error(char *message, size_t capacity);

/**
 * @brief Creates a new item list with capacity 'capacity'.
 *
 * @param list Item list structure to store the created list.
 * @param capacity Capacity of the item list.
 *
 * @return Given list.
 */
pl_item_list_t *pl_item_list_create(pl_item_list_t *list, size_t capacity);

/**
 * @brief Destroys the item list in 'item'.
 *
 * @param list Item list to be destroyed.
 *
 * @return Given list.
 */
pl_item_list_t *pl_item_list_delete(pl_item_list_t *list);

#ifdef __cplusplus
}
#endif

#endif /* PLUTO_H */
